/// emblem_generator();

var emblems = -1;

emblems[0] = spr_emblem_trade;
emblems[1] = spr_emblem_outpost;

var dice = irandom_range(0, array_length_1d(emblems)-1);

return emblems[dice];
