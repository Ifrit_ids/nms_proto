/// flicker_alpha(frame, frameArray);

var frames = argument1;

for(var i = 0; i < array_length_1d(frames); i++)
{
    if(argument0 == frames[i])
    {
        draw_set_alpha(0);
    }
    else draw_set_alpha(1);
}
