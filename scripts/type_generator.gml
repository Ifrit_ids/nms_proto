/// type_generator(int randomizer);

/*
 *      You know, just use a randomizer for randgen.
 */

randomize();
var types = -1;

types[0] = "HUB";
types[1] = "STATION";
types[2] = "COLONY";
types[3] = "OUTPOST";

var dice = irandom_range(0, array_length_1d(types)-1);

return types[dice];