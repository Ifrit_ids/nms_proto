/// equip_to_slot(slot, item);

var slot_item = argument0;
var selected_item = argument1;

// check if engine slot currently has anything equipped
if(slot_item == -1)
{
    slot_item.equipped = true;
    slot_item = selected_item;
}
else
{
    if(slot_item == selected_item)
    {
        slot_item.equipped = false;
        slot_item = -1;
    }
    else
    {
        slot_item.equipped = false;
        slot_item = selected_item;
        slot_item.equipped = true;
    }
}
