/// apply_gravity()
//notes:
//this script can easily handle several planets quite quickly. Run it once and all your forces are taken care of.
//the object calling this script needs to have the variable "mass" set as well, as this is factored into things.
//I'm sure theres plenty of room for improvement, feel free to use this wherever.
//gravitysource would preferable be a parent object you use? (so you can make oBlackHole, oPlanet, etc.)
with (gravitysource) { //with everything that has gravity
        
        mass = image_xscale //dosn't need to be dependent on image_scale
        
        if id = other.id  { } else { //this basically makes sure planets don't apply gravity onto themselfs
        var dis;
        dis = distance_to_object(other) //distance to the object calling this script (the one you want affected by gravity)
        
        var grav;
        grav = .001 //global gravity multiplier, you should probably set this as a globalvar
        
        ran = mass * 750 //total range that is affected by gravity
        
        other.ran = ran //i set this in the other object for later
        
        other.pmass = mass //This essentially tells the object how much the planet weighs PlanetMass
        
        if dis < ran { //if i'm farther away from the gravity source then it has any force, do nothing. (might bug out if you remove this limit)
            grav = (1 - (dis / ran)) * .10; //this does what the 10 if then statements below does (finds the amount of force based on distance)
            //if dis < ran * 1 { grav = .01 }
            //if dis < ran *.9 { grav = .02 }
            //if dis < ran *.8 { grav = .03 }
            //if dis < ran *.7 { grav = .04 }
            //if dis < ran *.6 { grav = .05 }
            //if dis < ran *.5 { grav = .06 }
            //if dis < ran *.4 { grav = .07 }
            //if dis < ran *.3 { grav = .08 }
            //if dis < ran *.2 { grav = .09 }
            //if dis < ran *.1 { grav = .1 } 
            with (other) { //this moves the script back to the object running the code
                //dg is a variable that i use to decide if planets should stick in place, or fly around. (DoGravity)
                if other.dg and object_index = oPlanet {} else { //this prevents planet's from affecting eachother, you should be safe to remove this.
                var force;
                force = grav * pmass * mass
                motion_add(point_direction(x,y,other.x,other.y,),force) //this finally applys the force, in the direction of towards the planet.
            }}
        }
    }    }
    