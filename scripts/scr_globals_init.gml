global.engine_speed = 0;
global.engine_max = 14;

global.hull = 100;
global.hull_max = 100;

global.fuel = 100;
global.max_fuel = 100;

global.page_x = 0;
global.page_y = 0;

global.station = -1;

/* inventory */
global.max_cargo = 16;
global.cargo = ds_list_create();

global.thrusters = -1;
global.hull_armor = -1;
global.shield = -1;
global.fuel_tank = -1;

global.hardpoints = ds_list_create();

/* inventory */

/* ignore */
global.connect_timer = 0;
global.connecting = false;
global.inrange = false;

global.grabbing = -1;
/* ignore */