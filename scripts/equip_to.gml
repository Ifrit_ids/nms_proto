/// equip_to(slot, item);

var slot = argument0;
var selected_item = argument1;

// check if engine slot currently has anything equipped
                if(slot == -1)
                {
                    // if empty
                    selected_item.equipped = true;
                    slot = selected_item;
                }
                else
                {
                    // if not, check if it's the same item
                    if(slot == selected_item)
                    {
                        slot.equipped = false;
                        slot = -1;
                    }
                    else
                    {
                        // if it isn't, then set the old item to unequipped, and the new one to equipped.
                        slot.equipped = false;
                        slot = selected_item;
                        slot.equipped = true;
                    }
                }

