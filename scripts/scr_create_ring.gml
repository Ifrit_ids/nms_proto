/// scr_create_ring( x, y, ring_color, rotspeed, segments, radius );

// noone for static position
var target_x = argument0;
var target_y = argument1;

// color_fix($ffffff);
var ring_color = argument2;

// rotation speed
var rotspeed = argument3;

// amount of dots orbiting
var segments = argument4;

// radius
var rad = argument5;

// selector object
var selector = instance_create(target_x,target_y,obj_ring);
selector.ring_color = ring_color;
selector.dots = segments;
selector.rotspeed = rotspeed;
selector.new_length = rad;

return selector;