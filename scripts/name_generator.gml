/// name_generator();

randomize();

var id_or_name = irandom_range(0, 4);

var first_syl = -1;

if(id_or_name > 3)
{
    // station named
    first_syl[0] = "LY";
    first_syl[1] = "FOR";
    first_syl[2] = "YA";
    first_syl[3] = "JAR";
    first_syl[4] = "QUA";
    first_syl[5] = "AE";
    first_syl[6] = "OR";
    first_syl[7] = "KOL";
    
    var second_syl = -1;
    
    second_syl[0] = "RIA";
    second_syl[1] = "DIAN";
    second_syl[2] = "RA";
    second_syl[3] = "DIS";
    second_syl[4] = "GIS";
    second_syl[5] = "NEA";
    second_syl[6] = "RIC";
    second_syl[7] = "BIS";
    
    var dice = irandom_range(0, array_length_1d(second_syl)-1);
    var first = first_syl[dice];
    var dice_second = irandom_range(0, array_length_1d(first_syl)-1);
    var second = second_syl[dice_second]

    return first + second;
}
else
{
    // station IDd
    var final = "";
    
    var alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var numbers = "1234567890";
    
    for(var i = 0; i < irandom_range(1, 2); i++)
    {
        var d = irandom_range(0, string_length(alphabets));
        final += string_char_at(alphabets, d);
    }
    
    final += "-";
    
    for(var i = 0; i < irandom_range(1, 3); i++)
    {
        var randy = irandom_range(0, 1);
        if(randy > 0)
        {
            var d = irandom_range(0, string_length(numbers));
            final += string_char_at(numbers, d);
        }
        else
        {
            var d = irandom_range(0, string_length(alphabets));
            final += string_char_at(alphabets, d);
        }
    }
    
    return final;
}
