/// scr_ship_menu();

var page_size = view_wview[0];

draw_set_colour(c_white);
draw_rectangle(page_size + 2, 36, (page_size * 2) - 4, page_size - 2, 1);

switch ( global.ship )
{
    case ship_hummingbird:
    {
        draw_sprite(ship_hummingbird, 0, page_size + 128, 128);
        break;
    }
}
